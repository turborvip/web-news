/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },
  //check auth device
  'POST /checkauth': { action: 'Login/checkAuthDevice' },
  //change and forgot password
  'POST /changepass': { action: 'Password/changePassWord' },
  'POST /forgotpassword': { action: 'Password/forgotPassword' },
  'POST /checkotppassword': { action: 'Password/postOTP' },
  //signup:
  'POST /signup': { action: 'SignUp/createAccount' },
  //login:
  'POST /login': { action: 'Login/postLogin' },
  'POST /checkotp': { action: 'Login/postOTP' },
  //logout:
  'POST /logout': { action: 'LogOut/logOut' },
  //category
  'POST /createcategory': { action: 'Category/createCategory' },
  'GET /listcategory': { action: 'Category/listCategory' },
  //news:
  'GET /detailnews/:idNews': { action: 'News/detailNews' },
  'GET /listnews/:idCategory': { action: 'News/listNewsByCategory' },
  'GET /homedata': { action: 'News/homeDataNews' },
  'GET /listnormalnews': { action: 'News/listNews' },
  'POST /createnews': { action: 'News/createNews' },
  'PUT /editnews': { action: 'News/editNews' },
  'DELETE /deletenews/:idNews': { action: 'News/deleteNews' },
  //uploads
  'POST /uploadimage': { action: 'Upload/uploadImage' },

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
