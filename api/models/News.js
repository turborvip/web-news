module.exports = {

    datastore: 'web_news',
    attributes: {
        caption: { type: 'string', allowNull: true, },
        idCategory: { type: 'string', allowNull: false, },
        image: { type: 'string', allowNull: true, },
        description: { type: 'string', allowNull: true, },
        text: { type: 'string', allowNull: true, columnType: 'text' },
        author: { type: 'string', allowNull: true, },
        viewer: { type: 'number', allowNull: false, },
        status: { type: 'boolean', allowNull: false, },
    },

};

