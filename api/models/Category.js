module.exports = {

    datastore: 'web_news',
    attributes: {
        name: { type: 'string', allowNull: true, },
        idParent: { type: 'string', allowNull: true },
        description: { type: 'string', allowNull: true, },
        status: { type: 'boolean', allowNull: false, },
    },

};

