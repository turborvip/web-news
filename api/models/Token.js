/**
 * Token.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  datastore: 'web_news',
  attributes: {
    idUser: { type: 'number', allowNull: true, isInteger: true, },
    name: { type: 'string', allowNull: true, isNotEmptyString: true, },
    token: { type: 'string', unique: true, allowNull: true, },
    expiredAt: { type: 'ref', columnType: 'datetime', },
    numberWrong: { type: 'number', isInteger: true },
    device: { type: 'string', allowNull: true, isNotEmptyString: true, },
    status: { type: 'boolean', allowNull: false, },
  },

};

