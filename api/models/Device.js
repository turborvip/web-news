/**
 * Auth.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    datastore: 'web_news',
    attributes: {
        idUser: { type: 'number', allowNull: true, isInteger: true, },
        name: { type: 'string', allowNull: true, isNotEmptyString: true },
        forbiddenTimeTo: { type: 'ref', allowNull: false, columnType: 'datetime' },
        numberLogin: { type: 'number', allowNull: false, },
        auth: { type: 'number', allowNull: false, },
        nearestOnline: { type: 'ref', allowNull: false, columnType: 'datetime' },
        status: { type: 'boolean', allowNull: false, },
    },
};

