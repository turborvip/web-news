const timeNow = require('./LoginController')

const logOut = async (req, res) => {
    try {
        const { idUser } = req.body;
        let deviceNow = req.header('user-agent');
        await Device.updateOne({
            name: deviceNow,
            idUser: idUser
        })
            .set({
                auth: 0,
                updatedAt: timeNow.timeNow(),
            });

        return res.json({ status: 202, msg: 'Logout success' });
    } catch (error) {
        console.log(error);
        return res.json({ status: 500, error: error });
    }

}

module.exports = {
    logOut
};