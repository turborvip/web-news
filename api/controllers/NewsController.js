
const moment = require('moment');
const dateNow = () => moment().format().toString();

const listNews = async (req, res) => {
    try {
        let caption = req.query.caption || "";
        let createdTime = req.query.createdTime ? req.query.createdTime : false;
        let fromTime = createdTime ? new Date(new Date(createdTime).getFullYear() + "-1-1") : new Date("2000-01-02");
        let toTime = createdTime ? new Date(new Date(createdTime).getFullYear() + "-12-31") : new Date();
        let description = req.query.description || "";
        let author = req.query.author || "";

        let size = req.query.size || 5;
        let page = req.query.page || 1;
        let offset = (parseInt(page) - 1) * parseInt(size);

        let countNews = await News.count();
        let totalPage = Math.ceil(countNews / parseInt(size))

        let newsDB = await News.find({
            where: {
                caption: { contains: caption },
                createdAt: {
                    '>=': fromTime,
                    "<=": toTime
                },
                description: { contains: description },
                author: { contains: author },
            },
            sort: 'id DESC',
        }).skip(offset).limit(size);

        let data = {
            status: 201,
            msg: "Get data news was success",
            auth: req.auth,
            User: req.dataUser,
            newsDB: newsDB,
            totalPage: totalPage,
            page: page
        }
        return res.json({ status: 200, data, msg: "Respone was success!" });

    } catch (error) {
        console.log(error);
        return res.json({ status: 500, msg: "Some thing wrong....!" })
    }

}

const detailNews = async (req, res) => {
    try {
        let idNews = req.params.idNews;
        let newsDetail = await News.findOne({
            where: {
                id: idNews
            }
        });
        await News.updateOne({ id: idNews })
            .set({
                viewer: ++newsDetail.viewer
            });

        let data = {
            status: 201,
            msg: "Get data news was success",
            auth: req.auth,
            User: req.dataUser,
            data: newsDetail,
        }
        return res.json({ status: 200, data, msg: "Respone was success!" });

    } catch (error) {
        console.log(error);
        return res.json({ status: 500, msg: "Some thing wrong....!" })
    }
}

const listNewsByCategory = async (req, res) => {
    try {
        let idCategory = req.params.idCategory || "";

        let filter = req.query.filter || 'createdAt DESC';
        let size = req.query.size || 2;
        let page = req.query.page || 1;
        let offset = (parseInt(page) - 1) * parseInt(size);
        console.log('filter', filter, '-----', page);
        let allNewsDB = await News.find({
            where: {
                idCategory: idCategory
            },
            sort: 'id ASC',
        });

        let newsDB = await News.find({
            where: {
                idCategory: idCategory
            },
            sort: filter,
        }).skip(offset).limit(size);

        let categoryDB = await Category.findOne({
            where: {
                id: idCategory
            },
        })

        let countNews = await allNewsDB.length;
        let totalPage = Math.ceil(countNews / parseInt(size))
        console.log('newsDB', newsDB);

        let data = {
            status: 201,
            msg: "Get data news was success",
            auth: req.auth,
            User: req.dataUser,
            data: [categoryDB, newsDB, totalPage, page],
        }
        return res.json({ status: 200, data, msg: "Respone was success!" });

    } catch (error) {
        console.log(error);
        return res.json({ status: 500, msg: "Some thing wrong....!" })
    }

}

const homeDataNews = async (req, res) => {
    try {
        let hotNews = await News.find({
            sort: 'viewer DESC',
        }).limit(5);

        let newContent = await News.find({
            sort: 'createdAt DESC',
        }).limit(7);

        let normalNews = await News.find({
            sort: 'id DESC',
        }).limit(5);

        let data = {
            status: 201,
            msg: "Get data news was success",
            auth: req.auth,
            User: req.dataUser,
            data: [normalNews, newContent, hotNews]
        }
        return res.json({ status: 200, data, msg: "Respone was success!" });

    } catch (error) {
        console.log(error);
        return res.json({ status: 500, msg: "Some thing wrong....!" })
    }
}

const createNews = async (req, res) => {
    try {
        const { caption, idCategory, image, description, author, text } = req.body;

        await News.create({
            "caption": caption,
            "idCategory": idCategory,
            "image": image,
            "text": text,
            "description": description,
            "author": author,
            "viewer": 0,
            "status": 1,
        });
        return res.json({ status: 200, msg: "Created news!..." })
    } catch (error) {
        res.json({ status: 500, msg: 'Wrong...', error: error })
    }
};

const editNews = async (req, res) => {
    try {
        const { newsID, caption, image, description, author, status } = req.body;
        await News.updateOne({ id: newsID })
            .set({
                caption: caption,
                image: image,
                description: description,
                author: author,
                status: status,
                updatedAt: dateNow(),
            });

        return res.json({ status: 201, msg: "Updated!..." });
    } catch (error) {
        res.json({ status: 500, msg: 'Wrong...', error: error })
    }
};

const deleteNews = async (req, res) => {
    try {
        const { idNews } = req.params;
        await News.destroyOne({ id: idNews });
        return res.json({ status: 201, msg: "Deleted!..." });
    } catch (error) {
        console.log(error);
        return res.json({ status: 500, msg: "Something wrong!..." })
    }
}


module.exports = {
    createNews,
    editNews,
    deleteNews,
    listNews,
    homeDataNews,
    listNewsByCategory,
    detailNews,
}