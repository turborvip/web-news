const bcrypt = require('bcryptjs');
const { timeNow, sendMail, getRandomInt, setExpiredTimeOTP, checkOTP } = require('./LoginController');

const changePassWord = async (req, res) => {
    try {
        const { idUser, password, rePassword } = req.body;
        const user = await User.findOne({
            where: {
                id: idUser,
            },
        });
        if (await bcrypt.compare(password, user.password)) {
            let encryptedPassword = await bcrypt.hash(rePassword, 10);
            console.log('encryptedPassword', encryptedPassword);
            await User.updateOne({
                id: idUser,
            })
                .set({
                    password: encryptedPassword,
                    updatedAt: timeNow()
                });
            return res.json({ status: 200, password: false, msg: 'Password was change!' });
        } else {
            return res.json({ status: 404, password: false, msg: 'Password was wrong!' });
        }

    } catch (error) {
        console.log(error);
        return res.json({ status: 500, error: error })
    }

}

const forgotPassword = async (req, res) => {
    try {
        const deviceNow = req.header('user-agent')
        const { username } = req.body;
        const user = await User.findOne({
            where: {
                username: username,
            },
        });

        if (user) {
            //Send otp
            let OTPServer = await getRandomInt(100000, 999999);
            await sendMail(user.email, OTPServer);
            const otp = await Token.findOne({
                where: {
                    idUser: user.id,
                    device: deviceNow,
                    expiredAt: {
                        ">=": timeNow(),
                    }
                },
            });
            if (otp) {
                await Token.destroy({ id: otp.id });
            }
            //create expireTime = currentTime + 60s
            let currentTime = new Date();
            let timeNeedConvert = currentTime.setMinutes((currentTime.getMinutes() + 1));
            await Token.create({
                "idUser": user.id,
                "name": "otp",
                "token": OTPServer,
                "expiredAt": setExpiredTimeOTP(timeNeedConvert),
                "numberWrong": 0,
                "device": deviceNow,
                "status": 1,
            });
            return res.json({ status: 202, idUser: user.id, otp: true, msg: 'Please check mail to check OTP!' });

        } else {
            return res.json({ status: 406, msg: "Username is available!" })
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: 500, error: error })
    }

}

const postOTP = async (req, res) => {
    try {
        const { OTPClient, idUser } = req.body;
        const user = await User.findOne({
            where: {
                id: idUser,
            },
        });
        let deviceNow = req.header('user-agent');
        if (OTPClient) {
            //check
            //-->rignt create token -> update device -> return
            let gearCheck = await checkOTP(+OTPClient, idUser, deviceNow);
            if (gearCheck == 0) {
                //OTP was expired
                return res.json({ status: 404, otp: 'expired', msg: "OTP was expired" });
            } else if (gearCheck == -1) {
                //OTP is right
                let newPassword = '123456abc';
                sendMail(user.email, newPassword, 'This is your new password!');
                //Update password
                let encryptedPassword = await bcrypt.hash(newPassword, 10);
                console.log('encryptedPassword', encryptedPassword);
                await User.updateOne({
                    id: idUser,
                })
                    .set({
                        password: encryptedPassword,
                        updatedAt: timeNow()
                    });
                return res.json({ status: 202, res: true, otp: true });
            } else if (gearCheck == -2) {
                return res.json({ status: 404, otp: true, otp: 'different device', msg: "You must enter OTP with the same device" });
            }
            else {
                //OTP wrong suitable for idUser
                return res.json({ status: 404, otp: false, msg: "OTP is wrong", numberWrong: gearCheck });
            }
            //-->wrong check number wrong ---- ect....
        }
    } catch (error) {
        console.log(error)
        return res.json({ status: 500, error: error, res: false });
    }
};

module.exports = {
    changePassWord,
    forgotPassword,
    postOTP,
}