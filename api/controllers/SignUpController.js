
const bcrypt = require('bcryptjs');

const createAccount = async (req, res) => {
    try {
        const { username, name, password, email } = req.body;

        if (username && name && password && email) {
            const usernameOld = await User.findOne({
                where: {
                    username: username
                },
            });
            const emailOld = await User.findOne({
                where: {
                    email: email
                },
            });
            // check username
            if (usernameOld) {
                res.json({ status: 406, msg: 'Username was available', username: false })
            } else {
                //check email
                if (emailOld) {
                    return res.json({ status: 406, msg: 'Email was available', password: false })
                } else {
                    //encode password by bcrypt
                    let encryptedPassword = await bcrypt.hash(password, 10);
                    await User.create({
                        "username": username,
                        "name": name,
                        "password": encryptedPassword,
                        "email": email,
                        "numberLogin": 0,
                        "status": 1,
                    });
                    //get id_user just created
                    let user = await User.findOne({ username: username }); n
                    //add device
                    await Device.create({
                        "idUser": user.id,
                        "name": req.headers['user-agent'],
                        "auth": 0,
                        "status": 1,
                    });
                    return res.json({ status: 201, msg: "Created user!..." })
                }
            }
        } else {
            return res.json({ status: 500, error: 'You must enter all infomation!' })
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: 500, msg: 'Wrong...', error: error })
    }
};

module.exports = {
    createAccount,
}