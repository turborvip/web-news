
const createCategory = async (req, res) => {
    try {
        const { name, idParent, description, status } = req.body;

        await Category.create({
            "name": name,
            "idParent": idParent,
            "description": description,
            "status": status,
        });
        return res.json({ status: 201, msg: "Created category!..." })
    } catch (error) {
        res.json({ status: 500, msg: 'Wrong...', error: error })
    }
};
const listCategory = async (req, res) => {
    try {
        let categoryDB = await Category.find().sort('id ASC')
        let data = {
            status: 201,
            msg: "Get data news was success",
            auth: req.auth,
            categoryDB,
        }
        return res.json({ status: 200, data, msg: "Respone was success!" });

    } catch (error) {
        console.log(error);
        return res.json({ status: 500, msg: "Some thing wrong....!" })
    }

}


module.exports = {
    createCategory,
    listCategory
}