/**
 * UploadController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

require('dotenv').config();
const dirname = require('path').resolve(sails.config.appPath, 'assets/images/avatar');
const moment = require('moment');
const textDate = moment().format().toString();

const uploadImage = (req, res) => {
    req.file('file').upload({
        // don't allow the total upload size to exceed ~100MB
        maxBytes: 100000000,
        // set the directory
        dirname: dirname
    }, async function (err, uploadedFile) {
        const locationOrigin = req.query.locationOrigin;
        const studentId = req.query.studentId;
        // if error negotiate
        if (err) return res.negotiate(err);
        // logging the filename
        let lengthfd = await uploadedFile[0].fd.split("\\").length - 1;
        let namefile = await uploadedFile[0].fd.split("\\")[lengthfd];
        let linkfile = await locationOrigin + '/images/avatar/' + namefile
        console.log("inf file", locationOrigin + '/images/avatar/' + namefile);
        await Students.updateOne({ id: studentId })
            .set({
                image: linkfile,
                updatedAt: textDate
            });
        return res.json({ status: 200, msg: "Updated!..." });
    })
}

module.exports = {
    uploadImage,
};

