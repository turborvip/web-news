require('dotenv').config();
const nodemailer = require("nodemailer");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const setExpiredTimeOTP = (timeNeedConvert) => {

    return moment(timeNeedConvert).format()
};

// Get time now
const timeNow = () => moment().format();

// Create OTP number
const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
};
// To update auth attr device
const authDeviceTrue = async (nameDevice, idUser) => {
    let deviceDB = await Device.findOne({
        where: {
            name: nameDevice,
            idUser: idUser
        },
    });
    await Device.updateOne({
        name: nameDevice,
        idUser, idUser
    })
        .set({
            auth: 1,
            updatedAt: timeNow(),
            numberLogin: +deviceDB.numberLogin + 1,
            nearestOnline: timeNow()
        });
}

// To send mail
const sendMail = async (toEmail, OTP, msg) => {
    try {
        let transporter = await nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: process.env.USERNAME_EMAIL,
                pass: 'qatoxbzhvptkfemg',
            },
        });

        await transporter.sendMail({
            from: process.env.USERNAME_EMAIL,
            to: toEmail,
            subject: msg || "Sending OTP for login sails project",
            // text: `OTP is : ${OTP}`,
            html: `<h1>${OTP}</h1>`,
        })
    } catch (error) {
        console.log(error);
    }
};

const checkDevice = async (email, checkDeviceBool, idUser, deviceNow) => {
    // console.log('parameter checkDevice', email, checkDeviceBool, idUser, deviceNow);
    if (checkDeviceBool) {
        return true
    } else {
        let OTPServer = await getRandomInt(100000, 999999);
        await sendMail(email, OTPServer)
        const otp = await Token.findOne({
            where: {
                idUser: idUser,
                expiredAt: {
                    ">=": timeNow(),
                }
            },
        });
        if (otp) {
            await Token.destroy({ id: otp.id });
        }
        //create expireTime = currentTime + 60s
        let currentTime = new Date();
        let timeNeedConvert = currentTime.setMinutes((currentTime.getMinutes() + 1));
        await Token.create({
            "idUser": idUser,
            "name": "otp",
            "token": OTPServer,
            "expiredAt": setExpiredTimeOTP(timeNeedConvert),
            "numberWrong": 0,
            "device": deviceNow,
            "status": 1,
        });

        return false
    }
};

const upDateDevices = async (idUser, deviceNow) => {
    console.log('update device');
    //Check device then add if it unavailable;

    await Device.create({
        "idUser": idUser,
        "name": deviceNow,
        "status": 1,
        "numberLogin": 0,
        "nearestOnline": timeNow(),
        "status": 1,
    });
};


const postLogin = async (req, res) => {
    try {
        const { username, password } = req.body;
        let user = await User.findOne({ username: username });
        let deviceNow = req.header('user-agent')
        if (user) {
            if (await bcrypt.compare(password, user.password)) {
                // find device and check is it available ? 
                let device = await Device.find({ idUser: user.id });
                let checkDeviceBool = false;
                for (let i = 0; i < device.length; i++) {
                    if (device[i].name == deviceNow) {
                        checkDeviceBool = true;
                        break;
                    }
                }
                // run checkDevice function...
                if (await checkDevice(user.email, checkDeviceBool, user.id, deviceNow)) {
                    // Create access Token
                    let data = {
                        id: user.id,
                        name: user.name,
                        email: user.email,
                    }
                    const accessToken = jwt.sign({ data: data }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1d' });
                    // Update auth device 
                    authDeviceTrue(deviceNow, user.id);
                    return res.json({ res: true, otp: 'success', user: data, accessToken, msg: 'Login was success' });
                } else {
                    return res.json({ status: 202, idUser: user.id, otp: 'pendding', msg: 'You logined by a new device so please check mail which was signup and enter the below input OTP!' });
                }
            } else {
                return res.json({ status: 404, password: false, msg: 'Password was wrong!' });
            }
        } else {
            return res.json({ status: 404, username: false, msg: 'Username was wrong!' });
        }

    } catch (error) {
        console.log(error)
        return res.json({ status: 500, error: error, res: false });
    }
};

const updateDeviceForbidden = async (idUser, deviceNow) => {

    let otpDB = await Token.findOne({ idUser: idUser, name: deviceNow });
    console.log('deviceDB', otpDB.numberWrong > 0);
    if ((otpDB.numberWrong % 5) === 0 && (otpDB.numberWrong / 5) > 0) {
        let currentTime = new Date();
        let timeNeedConvert = currentTime.setSeconds((currentTime.getSeconds() + 30 * otpDB.numberWrong));
        Device.updateOne({
            idUser: idUser,
            name: deviceNow
        })
            .set({
                forbiddenTimeTo: setExpiredTimeOTP(timeNeedConvert),
                status: 0,
                updatedAt: timeNow(),
            });
    }

}

const checkOTP = async (OTPClient, idUser, deviceNow) => {
    // console.log('OTPClient and idUser', OTPClient, '---', idUser)
    const otp = await Token.findOne({
        where: {
            token: OTPClient,
            idUser: idUser,
            expiredAt: {
                ">=": timeNow(),
            }
        },
    });
    if (otp) {
        // check device
        if (otp.device == deviceNow) {
            // otp is right
            // device right
            return -1;
        } else {
            return -2;
        }

    } else {
        //otp is wrong
        let checkUpdatetoken = await updateToken(idUser);
        if (checkUpdatetoken == 0) {
            return 0
        } else {
            return checkUpdatetoken;
        }
    }
};
const updateToken = async (idUser) => {
    const otp = await Token.findOne({
        where: {
            idUser: idUser,
            expiredAt: {
                ">=": timeNow(),
            }
        },
    });
    if (otp) {
        // available otp suitable for idUser
        let numberWrong = parseInt(otp.numberWrong) + 1
        console.log('numberWrong', numberWrong)
        await Token.updateOne({
            idUser: idUser,
            expiredAt: {
                ">=": timeNow(),
            }
        })
            .set({
                numberWrong: numberWrong,
                updatedAt: timeNow()
            });
        return numberWrong;
    } else {
        // Not available otp suitable for idUser
        return 0;
    }
};
const postOTP = async (req, res) => {
    try {
        const { OTPClient, idUser } = req.body;
        let deviceNow = req.header('user-agent');
        let user = await User.findOne({ id: idUser });

        if (OTPClient) {
            //check
            //-->rignt create token -> update device -> return
            let gearCheck = await checkOTP(parseInt(OTPClient), idUser, deviceNow);
            if (gearCheck == 0) {
                //OTP was expired
                return res.json({ status: 404, otp: 'expired', msg: "OTP was expired" });
            } else if (gearCheck == -1) {
                //OTP is right
                await upDateDevices(idUser, deviceNow);
                await authDeviceTrue(deviceNow, idUser);
                let data = {
                    id: user.id,
                    name: user.name,
                    email: user.email,
                }
                const accessToken = jwt.sign({ data }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1d' });
                return res.json({ status: 202, res: true, user: data, accessToken, msg: 'OTP is right -> next()' });
            } else if (gearCheck == -2) {
                return res.json({ status: 404, otp: 'different device', msg: "You must enter OTP with the same device" });
            }
            else {
                //OTP is not suitable for idUser
                return res.json({ status: 404, otp: false, msg: "OTP is wrong", numberWrong: gearCheck });
            }
            //-->wrong check number wrong ---- ect....

        }
    } catch (error) {
        console.log(error)
        return res.json({ status: 500, error: error, res: false });
    }
};

const checkAuthDevice = async (req, res) => {

    try {
        const { idUser } = req.body;
        let deviceNow = req.header('user-agent');
        console.log("go to checkAuthDevice.....")

        const deviceDB = await Device.findOne({
            where: {
                name: deviceNow,
                idUser: idUser,
            },
        });
        if (deviceDB.auth) {
            return res.json({ status: 202, res: true, data: deviceDB.auth });
        } else {
            return res.json({ status: 500, error: error, res: false });

        }
    } catch (error) {
        console.log('err', error);
        return res.json({ status: 500, error: error, res: false });
    }
}

module.exports = {
    postLogin,
    postOTP,
    timeNow,
    checkAuthDevice,
    sendMail,
    getRandomInt,
    setExpiredTimeOTP,
    checkOTP,
    sendMail,
};

