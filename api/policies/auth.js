const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    let authorizetionHeader = req.headers.authorization;
    let token = authorizetionHeader ? authorizetionHeader.split(' ')[1] : null;
    if (token == null) {
        console.log("không có token!..");
        return res.json({ auth: false });
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        if (err) {
            err = {
                name: 'JsonWebTokenError',
                message: 'jwt signature is required or expired',
            }
            console.log("err", err)
            return res.json({ auth: false });
        }
        req.auth = true;
        req.dataUser = data;
        req.token = token;
        next();
    });
};