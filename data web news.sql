-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: web_news
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archive`
--

DROP TABLE IF EXISTS `archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `archive` (
  `id` int NOT NULL AUTO_INCREMENT,
  `createdAt` bigint DEFAULT NULL,
  `fromModel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `originalRecord` longtext COLLATE utf8_unicode_ci,
  `originalRecordId` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive`
--

LOCK TABLES `archive` WRITE;
/*!40000 ALTER TABLE `archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idParent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('2022-06-08 10:53:05','2022-06-08 10:53:05',1,'Xuất bản','','Publish',1),('2022-06-08 10:53:29','2022-06-08 10:53:29',2,'Xã hội','','Social',1),('2022-06-08 10:54:35','2022-06-08 10:54:35',3,'Thế giới ','','World',1),('2022-06-08 10:54:57','2022-06-08 10:54:57',4,'Kinh Doanh','','Bussinees',1),('2022-06-08 10:55:15','2022-06-08 10:55:15',5,'Công nghệ','','Technology',1),('2022-06-08 10:55:58','2022-06-08 10:55:58',6,'Giải trí','','Entertainment',1),('2022-06-08 10:56:21','2022-06-08 10:56:21',7,'Sức khỏe','','Heathy',1),('2022-06-08 10:56:39','2022-06-08 10:56:39',8,'Thể thao','','Sport',1),('2022-06-08 10:57:10','2022-06-08 10:57:10',9,'Du Lịch','','Travel',1),('2022-06-08 10:57:29','2022-06-08 10:57:29',10,'Đời sống','','Life',1),('2022-06-08 10:57:51','2022-06-08 10:57:51',11,'Pháp luật','','Law',1),('2022-06-08 10:58:37','2022-06-08 10:58:37',12,'Đô Thị','2','Urban',1),('2022-06-08 10:58:55','2022-06-08 10:58:55',13,'Giao thông','2','Trafic',1),('2022-06-08 10:59:30','2022-06-08 10:59:30',14,'Người Việt 4 phương','3','around the world',1),('2022-06-08 11:00:18','2022-06-08 11:00:18',15,'Blockchain','5','Hot',1),('2022-06-08 11:00:46','2022-06-08 11:00:46',16,'Ô tô','13','Car',1),('2022-06-08 11:03:14','2022-06-08 11:03:14',17,'Xe máy','13',NULL,1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device` (
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `idUser` double DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forbiddenTimeTo` datetime DEFAULT NULL,
  `numberLogin` double DEFAULT NULL,
  `auth` double DEFAULT NULL,
  `nearestOnline` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES ('2022-06-13 09:57:35','2022-06-13 09:57:35',1,1,'PostmanRuntime/7.29.0',NULL,0,0,NULL,1),('2022-06-13 10:41:18','2022-06-18 23:19:16',8,1,'postman1',NULL,6,1,'2022-06-18 23:19:16',1),('2022-06-13 15:09:25','2022-06-13 15:44:40',9,2,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36',NULL,2,0,'2022-06-13 15:33:36',1),('2022-06-15 11:44:46','2022-06-20 08:50:14',11,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',NULL,10,1,'2022-06-20 08:50:14',1),('2022-06-15 14:36:34','2022-06-18 23:22:11',12,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',NULL,7,1,'2022-06-18 23:22:11',1),('2022-06-18 23:08:08','2022-06-18 23:08:08',13,1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.124 Safari/537.36 Edg/102.0.1245.44',NULL,1,1,'2022-06-18 23:08:08',1);
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idCategory` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `viewer` double DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES ('2022-06-08 10:41:16','2022-06-20 08:52:45',1,'Thiếu tá CSGT bị người vi phạm tông gãy chân','1','https://znews-photo.zingcdn.me/Uploaded/qxwpzdjwp/2022_05_26/280.gif','Thiếu tá CSGT bị nam tài xế xe máy tông ngã làm gãy chân sau khi ra hiệu lệnh dừng xe ở thành phố Ninh Bình.',NULL,'Hồng Quang',140,1),('2022-06-08 10:43:07','2022-06-20 08:52:44',2,'Khi cái đẹp dẫn đến tự hủy','1','https://znews-photo.zingcdn.me/w960/Uploaded/jotnhg/2020_10_26/Kim_cac_tu.jpg','Gắn cái đẹp với mầm ác, Mishima khiến nhiều người tranh luận nhưng không ai có thể phủ nhận tầm ảnh hưởng của ông với văn chương thế giới.',NULL,'Hồng Quang',56,1),('2022-06-08 10:43:33','2022-06-20 08:52:46',3,'Mua sắm vô độ ảnh hưởng gì đến môi trường?','1','https://znews-photo.zingcdn.me/w660/Uploaded/mzjyy/2022_06_08/Fabrice_Monteiro_2_.jpg','Việc sản xuất đồ may mặc và vải vóc trên toàn cầu đã tạo ra hàng triệu triệu tấn chất độc hại. Con người mua đồ mới nhanh chóng thế nào thì cũng vứt bỏ đồ cũ với tốc độ như thế.',NULL,'Hồng Quang',12,1),('2022-06-08 10:44:01','2022-06-12 18:44:11',4,'Những món ngon đậm hồn quê xứ','1','https://znews-photo.zingcdn.me/w360/Uploaded/dqmblcvo/2022_06_06/Banh_Nam.jpg','Qua những món ăn dung dị thấm đậm hồn quê xứ, các tác giả bày tỏ nghĩa nặng tình sâu của mình với quê hương.',NULL,'Hồng Quang',12,1),('2022-06-08 10:44:25','2022-06-16 16:56:57',5,'Điểm hẹn hò lý tưởng ở TP.HCM','1','https://znews-photo.zingcdn.me/w360/Uploaded/mzjyy/2022_06_06/Ng_Ky_Anh_2_.jpg','Hàng ngày đi qua sở thú TP.HCM, không biết tự lúc nào, tôi bỗng nặng lòng với nơi đây. Có lẽ đây là nơi lý tưởng để hẹn hò trong thành phố.',NULL,'Hồng Quang',37,1),('2022-06-08 10:44:45','2022-06-13 15:36:33',6,'Thế giới huyền bí dưới biển sâu','1','https://znews-photo.zingcdn.me/w360/Uploaded/jotnhg/2022_06_06/Screenshot_1.jpg','Thế giới động vật nơi biển cả có những giống loài đa dạng với đặc tính riêng có thể khiến chúng ta ngỡ ngàng về nó, như trường hợp bạch tuộc và san hô.',NULL,'Hồng Quang',10,1),('2022-06-08 10:45:14','2022-06-13 16:03:30',7,'Loài cá đực sinh con','1','https://znews-photo.zingcdn.me/w360/Uploaded/jotnhg/2022_06_04/xg4a3g8u3xv21.jpg','Cá ngựa là một trong những loài cá mang đặc tính khác lạ, con đực mang thai thay cho con cái.',NULL,'Hồng Quang',6,1),('2022-06-08 10:46:28','2022-06-18 23:26:21',8,'Sự trở lại của các lễ hội truyện tranh','1','https://znews-photo.zingcdn.me/w360/Uploaded/mzjyy/2022_05_11/thumb.jpg','Trong khi các sự kiện lớn về truyện tranh bắt đầu tái xuất với hình thức trực tiếp, các chương trình độc lập có quy mô vừa và nhỏ cũng mở cửa trở lại theo đúng lịch trình.',NULL,'Hồng Quang',17,1),('2022-06-08 10:47:13','2022-06-13 16:03:31',9,'Khám phá mọi không gian bằng trí tuệ nhân tạo','1','https://znews-photo.zingcdn.me/w360/Uploaded/mzjyy/2022_05_30/EPFL_2_.jpg','Bằng cách kề vai sát cánh cùng trí tuệ nhân tạo, chúng ta sẽ có cơ hội được khám phá mọi không gian mà hoàn toàn không chịu sự chi phối bởi điều gì.',NULL,'Hồng Quang',2,1),('2022-06-08 10:47:53','2022-06-16 16:56:38',10,'Ancelotti và những phát minh về chiến thuật','1','https://znews-photo.zingcdn.me/w360/Uploaded/dqmblcvo/2022_06_03/Ancelotti.png','“Bạn phải thường xuyên thay đổi sơ đồ thi đấu để thay thế các cầu thủ chấn thương hoặc để thích ứng với các cầu thủ mới của đội bóng”, Ancelotti chia sẻ trong tự truyện.',NULL,'Hồng Quang',6,1),('2022-06-08 10:50:07','2022-06-13 14:25:56',11,'Tác phẩm thiếu nhi hiếm hoi của các nhà văn nổi tiếng','1','https://znews-photo.zingcdn.me/w360/Uploaded/mzjyy/2022_06_01/Anh_2_Kit_kit_bum_bum.jpg','Nhiều tác giả có tiếng tăm, tưởng chỉ sáng tác cho độc giả trưởng thành lại tạo bất ngờ với người đọc khi viết cho trẻ nhỏ.',NULL,'Hồng Quang',5,1),('2022-06-08 11:11:10','2022-06-18 23:10:24',12,'Thất bại đau đớn nhất của Alex Ferguson','1','https://znews-photo.zingcdn.me/w960/Uploaded/dqmblcvo/2022_06_07/sir_alex_ferguson_kanan_berteriak_pada_roberto_mancini_120501083622_447.jpg','“Trong tất cả thất bại mà tôi đã phải chịu đựng, không thất bại nào có thể tệ hơn việc mất chức vô địch về tay City”, Sir Alex bộc bạch trong cuốn hồi ký.',NULL,'Thành Đạt',36,1),('2022-06-12 19:14:42','2022-06-20 08:52:55',13,'Hàng nghìn người dân Hà Tĩnh đổ xô xuống đầm bắt cá','1','https://znews-photo.zingcdn.me/w660/Uploaded/qhj_dvoahficbu/2022_06_12/14.jpg','Sau tiếng trống khai hội, hàng nghìn người dân ở Hà Tĩnh ùn ùn lao xuống đầm nước rộng lớn để đánh bắt cá. Đây là lễ hội truyền thống của địa phương tồn tại gần 300 năm qua.',NULL,'Phạm Trường',17,1),('2022-06-15 15:11:58','2022-06-20 08:53:07',14,'Nghĩa địa san hô trong khu bảo tồn vịnh Nha Trang','2','https://znews-photo.zingcdn.me/w960/Uploaded/nugzrd/2022_06_15/4.jpg','Hàng nghìn m2 đáy biển khu vực vùng lõi Khu bảo tồn biển vịnh Nha Trang chất đống san hô chết. Một số vị trí đã sạch dấu vết san hô.','DU LỊCH\n\nNghĩa địa san hô trong khu bảo tồn vịnh Nha TrangXuân Hoát Thứ tư, 15/6/2022 11:44 (GMT+7)Hàng nghìn m2 đáy biển khu vực vùng lõi Khu bảo tồn biển vịnh Nha Trang chất đống san hô chết. Một số vị trí đã sạch dấu vết san hô.\n\nSan ho khu bao ton bi tay trang anh 1\nĐảo Hòn Mun - vùng lõi Khu bảo tồn biển vịnh Nha Trang, là nơi được bảo vệ nghiêm ngặt và là khu vực có hệ sinh thái san hô đẹp bậc nhất Việt Nam với nhiều loài đặc sắc.\n\nSan ho khu bao ton bi tay trang anh 2\nTuy nhiên, hiện nơi này san hô chết khoảng 70-80%. Dưới đáy biển, nhiều khu vực, mỗi khu có hàng trăm m2 sạch bóng san hô hoặc xác san hô chết chất đống.\n\nSan ho khu bao ton bi tay trang anh 3\nĐáy biển phủ một màu xám xịt. San hô chết gây ảnh hưởng nghiêm trọng đến hệ sinh thái nơi này. Nhiều loài thủy sản đã rời đi nơi khác trú ngụ.\n\nSan ho khu bao ton bi tay trang anh 4\n\"Rất đau xót. Mình làm nghề lặn 10 năm, giờ chứng kiến cảnh san hô chết trắng đáy biển thật sự buồn lắm. Có thể 50-60 năm nữa chưa chắc phục hồi được như trước\", anh Lê Đình Trí, một huấn luyện viên lặn, nói.\n\nSan ho khu bao ton bi tay trang anh 5\nSan hô chết chất đống dưới đáy biển khu vực phía nam đảo Hòn Mun.\n\nSan ho khu bao ton bi tay trang anh 6\n\"Vị trí này san hô bị \'tẩy trắng\' sau đợt bão tháng 9/2021. Dưới đáy phủ đầy san hô chết, còn trên bờ sóng cũng đánh dạt chất hàng đống\", ông Cao Đức Đại, cán bộ Ban quản lý vịnh Nha Trang, cho biết.\n\nSan ho khu bao ton bi tay trang anh 7\nTiếp tục lặn khu vực bắc đảo Hòn Mun, chúng tôi chứng kiến đáy biển gần như sạch bóng san hô.\n\nSan ho khu bao ton bi tay trang anh 8\n\"Khu vực này trước đây có hệ sinh thái san hô tuyệt đẹp. Hàng trăm nghìn du khách cũng mê mẩn khi lặn ngắm san hô ở đây nhưng giờ đáy biển không còn gì\", anh Trí nói.\n\nSan ho khu bao ton bi tay trang anh 9\nĐáy biển khu vực bắc Hòn Mun chỉ còn trơ lại những mỏm đá, lác đác vài con cầu gai, các loại cá gần như biến mất hoàn toàn vì không còn nơi trú ngụ.\n\nSan ho khu bao ton bi tay trang anh 10\nTrên bờ hàng tấn san hô chết bị sóng đánh dạt.\n\nSan ho khu bao ton bi tay trang anh 11\n\"San hô bị sóng đánh lên bờ từ cuối năm 2021 khi cơn bão số 9 quét qua\", ông Đại thông tin.\n\nSan ho khu bao ton bi tay trang anh 12\nNgoài san hô sừng, nhiều khối san hô tảng cũng bị sóng đánh dạt lên bờ quanh khu vực đảo Hòn Mun.\n\nSan ho khu bao ton bi tay trang anh 13\n\"Du khách vẫn chọn hình thức lặn biển ngắm san hô ở khu vực Hòn Mun. Tuy nhiên, nhiều người tỏ ra buồn khi chứng kiến san hô chết quá nhiều, không còn đẹp như xưa\", một huấn luyện viên lặn cho biết.','Xuân Hoát ',14,1),('2022-06-15 15:30:27','2022-06-15 15:30:55',15,'Phản ứng trái chiều về phim Trịnh Công Sơn','2','https://znews-photo.zingcdn.me/w480/Uploaded/wpdhnwhnw/2022_06_15/avalanty.jpg','Hai tác phẩm tái hiện chân dung, cuộc đời của người nhạc sĩ Trịnh Công Sơn đã và đang nhận nhiều phản ứng trái chiều từ khán giả yêu phim.','Những khán giả mộ điệu nhạc Trịnh đã chờ đợi hơn hai năm để có thể chiêm ngưỡng hình ảnh người nhạc sĩ tài ba của Việt Nam bước lên màn ảnh rộng. Ngay từ khi công bố dự án, Em và Trịnh đã trở thành tựa phim đầy kỳ vọng, với mức đầu tư khoảng 50 tỷ đồng (theo công bố từ nhà sản xuất).\n\nTrước ngày phát hành, nhà sản xuất bất ngờ công bố phát hành song song hai bản phim mang tên Em và Trịnh (136 phút) cùng Trịnh Công Sơn (95 phút). Đây là câu chuyện vô tiền khoáng hậu trong lịch sử của điện ảnh Việt Nam.\n\nKhông chỉ gây tranh cãi từ khâu phát hành, sau gần một tuần công chiếu, chất lượng phim cũng nhận không ít phản ứng trái chiều từ công chúng.\n\nphim trinh cong son,  em va trinh anh 1\nPhim đầy rẫy sạn\n\"Em và Trịnh giống như một MV ca nhạc bị cắt ghép nham nhở, thất bại trong mọi mục tiêu mà nó hướng đến. Chẳng hề mang đến câu chuyện lãng mạn và cũng không miêu tả được chân dung hay âm nhạc của Trịnh Công Sơn. Thậm chí phim còn giống đang bêu xấu vị nhạc sĩ tài danh này. Mọi khâu của bộ phim đều ở dưới mức trung bình, từ đạo diễn đến dựng phim, cả âm nhạc, nhưng tệ nhất và chịu trách nhiệm lớn nhất là kịch bản\", tác giả Phan Cao Hoài Nam mở đầu bài viết của mình khi đánh giá tác phẩm Em và Trịnh.\n\nTheo tác giả, bộ phim đầy rẫy \"sạn\", có thể kể đến từ kịch bản phim ôm đồm, chi tiết rời rạc, thiếu sự kết nối. Diễn xuất của Avin Lu trong vai Trịnh Công Sơn quá mờ nhạt, không thể bật nổi lên chân dung của vị nhạc sĩ tài danh. Ngoài ra, nhiều đoạn tư liệu về chiến tranh đột ngột chèn vào mạch phim tạo cảm giác khiên cưỡng, không phù hợp.\n\n\nphim trinh cong son,  em va trinh anh 2phim trinh cong son,  em va trinh anh 3\nDiễn xuất của Avin Lu còn nhiều hạn chế. Ảnh: ĐPCC.\nGiống Hoài Nam, nhiều khán giả mê phim, yêu nhạc Trịnh cùng chung nhận xét khi theo dõi hai tác phẩm phát hành song song.\n\nĐiều khiến khán giả Nguyễn Lan Anh (đến từ Huế) băn khoăn đến từ việc các diễn viên trong phim nói giọng Huế không giống ngoài đời. Đặc biệt, nhân vật chính là Trịnh Công Sơn (Avin Lu thủ vai) trong một số phân đoạn còn nói giọng Bắc khiến người xem cảm giác hụt hẫng, khó thuyết phục.\n\n\"Ngay từ khi công bố dàn diễn viên, tôi đã thấy Avin Lu không phải là lựa chọn phù hợp. Tạo hình của nam diễn viên này nhìn qua đã không thấy có nét nào giống với cố nhạc sĩ Trịnh Công Sơn. Và khi phim công chiếu, nhìn nhận của tôi về diễn viên này vẫn không hề sai. Đài từ kém, diễn xuất còn nhiều hạn chế, thậm chí những nét biểu cảm trên khuôn mặt cũng sống sượng\", tài khoản @vuquang bình luận.\n\nTrên nhiều diễn đàn, mạng xã hội, khán giả cũng bình luận sôi nổi về những điểm được cũng như hạn chế của tác phẩm.\n\nTrong đó, người hâm mộ tập trung nhận xét về việc nhà sản xuất chiêu trò khi công chiếu một lúc hai bản phim cùng chung kịch bản, góc nhìn. Phim Trịnh Công Sơn thậm chí được xem là bản thu gọn của Em và Trịnh, không tạo ra sự khác biệt như lời gợi mở ban đầu từ ông Lương Công Hiếu, đại diện phía sản xuất. Nhiều người hâm mộ sau khi ra rạp còn không biết mình đang xem Trịnh Công Sơn hay Em và Trịnh.\n\nHọ cho rằng nhà sản xuất, phát hành tính đến bài toán về doanh thu mà không chú trọng chất lượng phim. Để rồi, kết quả cuối cùng là một trong hai bản thất thu và buộc phải rời rạp từ ngày 17/6.\n\nTheo khán giả Phan Lộc (35 tuổi), hạn chế lớn nhất của phim là kịch bản còn rối rắm, tham lam về chi tiết. Cụ thể, những mối tình giữa Trịnh Công Sơn và các người đẹp như Dao Ánh, Michiko, Bích Diễm... không tạo ra sự sâu sắc hoặc cảm xúc day dứt, thu hút cho người xem.\n\nMột số chi tiết khác như ánh mắt đờ đẫn của nhân vật chính khi thấy Bích Diễm, Dao Ánh rồi Thanh Thúy, cảnh cha của Dao Ánh nói với con gái về Trịnh Công Sơn trước chuyến tàu hoặc các em gái của nhạc sĩ núp sau cửa sổ để theo dõi anh trai và Dao Ánh tâm tình đều khiến người xem có cái nhìn khác (theo hướng tiêu cực) về nghệ sĩ Trịnh Công Sơn.\n\nNgoài ra, kết của phim cũng bị chê gượng ép, hụt hẫng và không thỏa mãn số đông khán giả.\n\nĐiểm sáng hiếm hoi\nNhững lời khen của khán giả dành cho phim chủ yếu đến từ sự đầu tư về bối cảnh, trang phục và nhạc phim.\n\nTài khoản @nguyenhuong nhận xét sự đầu tư khoảng 50 tỷ đồng của nhà sản xuất dành cho hai bộ phim là hoàn toàn phù hợp. Những khung cảnh xứ Huế trong cơn mưa, con đường rợp bóng cây xanh, căn nhà cũ của cố nhạc sĩ, không gian núi rừng bao la hùng vĩ, ngôi nhà lụp xụp của thầy giáo Trịnh hay mái trường với những học sinh nơi miền núi, các tụ điểm âm nhạc... đều được đạo diễn, nhà sản xuất tái hiện công phu, đầy chất thơ.\n\nTrang phục của dàn diễn viên cũng được chú trọng để phản ánh một phần công việc, tính cách và trải dài theo tuyến tính thời gian. Theo đạo diễn Phan Gia Nhật Linh, ê-kíp sử dụng 700 bộ phục trang với 3.000 diễn viên quần chúng.\n\n\nphim trinh cong son,  em va trinh anh 4\nCác bối cảnh phim được dàn dựng công phu. Ảnh: ĐPCC.\nĐặc biệt, nhân vật Trịnh Công Sơn được tái hiện trải dài 3 thập kỷ, đòi hỏi sự kỹ lưỡng về phục trang, tạo hình. Nhà thiết kế Thủy Nguyễn - phụ trách phần trang phục cho phim - phải thốt lên: \"Chưa bao giờ có nhân vật nào nhiều phục trang như Trịnh Công Sơn\".\n\nVề phần âm nhạc, dưới bàn tay của nhạc sĩ Đức Trí mang lại màu sắc mới mẻ cho nhạc Trịnh qua giọng hát của Bùi Lan Hương, Avin Lu. Âm nhạc hòa hợp với cảnh phim, tạo độ chân thực, sâu lắng.\n\nKhán giả mê nhạc Trịnh có cơ hội thưởng thức nhiều nhạc phẩm trường tồn với thời gian và đầy tính triết lý.\n\nBên cạnh đó, diễn xuất của Hoàng Hà (vai Dao Ánh) và Bùi Lan Hương (vai Lệ Mai) nhận nhiều lời khen.\n\n\"Em Dao Ánh là gương mặt sáng của phim. Diễn xuất thông minh và tự nhiên. Mỗi cảnh em xuất hiện là sáng bừng. Mình thích cách diễn của em Ánh. Là gương mặt mới nhưng nhiều tiềm năng. Điện ảnh cần những gương mặt và nét diễn xuất này\", đạo diễn Bảo Nhân nhận xét.\n\nNói về diễn xuất của Bùi Lan Hương, anh chia sẻ: \"Bùi Lan Hương diễn như không diễn. Hương có đôi mắt như chứa bầu trời cảm xúc bên trong. Là ca sĩ đóng phim duy nhất mà khán giả không nhiều nghi ngại. Bùi Lan Hương nghiêm túc và có suy nghĩ rõ ràng cho vai diễn của mình\".\n\nPhim có đạt mốc doanh thu 100 tỷ đồng?\nTính đến ngày 15/6, Em và Trịnh ghi nhận mức doanh thu hơn 23 tỷ đồng, theo thống kê từ Box Office Vietnam. Còn Trịnh Công Sơn chỉ đạt trên 1,6 tỷ đồng và sẽ rời rạp chiếu từ ngày 17/6.\n\nNhư vậy, kế hoạch của nhà sản xuất để cải thiện doanh thu từ việc đưa hai tác phẩm phát hành song song đã đổ bể. Thời gian tới, nhà rạp sẽ tập trung suất chiếu cho Em và Trịnh.\n\nĐại diện của Lotte Cinema trao đổi với Zing nhà rạp kỳ vọng phim sẽ có mức doanh thu trên 100 tỷ đồng sau khi rời rạp.\n\n\"Lượng khách đến rạp mua vé xem phim Em và Trịnh vẫn tăng theo từng ngày. Chúng tôi đang tăng cường suất chiếu cho tác phẩm. Hy vọng bộ phim có doanh thu vượt 100 tỷ đồng, tạo ra cú hích cho phim Việt trong thời gian tới\", ông Đoàn Thạch Cương - Giám đốc kinh doanh của Lotte Cinema cho biết.\n\nphim trinh cong son,  em va trinh anh 5\nPhim Trịnh Công Sơn rời khỏi rạp chiếu vì doanh thu thấp. Ảnh: ĐPCC.\nCùng quan điểm, đại diện của Galaxy chia sẻ để có thể hòa vốn, tác phẩm Em và Trịnh phải có doanh thu trên 100 tỷ đồng. Nhà rạp cũng kỳ vọng đứa con tinh thần của Phan Gia Nhật Linh tạo ra con số doanh thu lớn, tiếp thêm động lực cho nhà rạp lẫn giới làm phim trong bối cảnh thị trường còn nhiều khó khăn.\n\n\"Phim đầu tư quá nhiều công sức, tiền bạc, mồ hôi. Cả giới làm phim đang nhìn vào tác phẩm này để mạnh dạn đầu tư lớn vào những dự án tương tự. Đây là canh bạc quá lớn khi tái hiện một chân dung nghệ sĩ có thật. Ai cũng mong phim thắng\", người này tiết lộ.\n\nTuy nhiên, từ góc nhìn của nhà phê bình phim Nguyễn Phong Việt, anh cho rằng căn cứ vào doanh thu hiện tại, số suất chiếu, Em và Trịnh chỉ có thể đạt mức doanh thu khoảng 70 tỷ đồng.\n\n\"Đối tượng khán giả của bộ phim không phải đại chúng. Tôi nghĩ để tạo ra sự đột biến về doanh thu hoặc cột mốc 100 tỷ đồng là rất khó\", anh nói thêm.','Hoàng Yến',2,1),('2022-06-15 15:33:21','2022-06-18 23:10:28',16,'Một cái chạm nhẹ cũng đủ giết chết san hô','9','https://znews-photo.zingcdn.me/w360/Uploaded/zngure/2022_06_14/barriere_de_corail_bali_content.jpg','Nếu không cẩn thận chạm vào san hô, chúng sẽ bị ảnh hưởng hoặc chết. Bỏ lại rác hay vô tình đánh rơi các vật dụng nhỏ cũng là yếu tố làm hại sinh vật này.','Rạn san hô là một trong những món quà của thiên nhiên. Cấu trúc carbon tuyệt đẹp này không chỉ làm tăng thêm vẻ đẹp của đại dương mà còn đóng vai trò quan trọng trong việc duy trì sự cân bằng hệ sinh thái. Chúng là nơi trú ngụ của nhiều loài cá nhỏ.\n\n\nbao ton ran san ho anh 1\nẢnh: National Geographic.\nRạn san hô là gì?\nTrên thực tế, một loài san hô đơn lẻ được tạo thành từ vô số động vật nhỏ bé được gọi là polyp.\n\nPolyp có thể nhỏ như một hạt cát hoặc có thể sở hữu kích thước lên đến 10 inch (hơn 25 cm). Chúng hút canxi cacbonat từ nước và kết tụ lại với nhau để tạo thành các rạn san hô.\n\nVì san hô là sinh vật sống, chúng cần nguồn thức ăn để tồn tại. Một số ăn cá nhỏ và sinh vật phù du, phần còn lại ăn tảo phát triển trên lớp ngoài. San hô mở rộng bằng cách sinh ra trứng san hô.\n\nCác rạn san hô có thể sống đến hàng nghìn năm tuổi. Chúng là những sinh vật nhạy cảm. Yếu tố nhỏ như nhiệt độ nước thay đổi, tiếp xúc quá mức với tia nắng mặt trời hoặc sóng mạnh đều có thể giết chết chúng. Ngoài ra, san hô cũng có thể bị ăn bởi nhiều sinh vật biển khác.\n\nTầm quan trọng\nCác rạn san hô có thể chỉ bao phủ tỷ lệ rất nhỏ dưới đáy đại dương thế giới. Tuy nhiên, gần 1/4 các sinh vật biển được rạn san hô bảo vệ và nuôi dưỡng trong giai đoạn phát triển ban đầu.\n\nCác rạn san hô cũng bảo vệ bờ biển khỏi tác động của thiên tai và làm tăng vẻ đẹp cảnh quan dưới đại dương.\n\n\nbao ton ran san ho anh 2\nẢnh: International Coral Reef Initiative.\nTuy nhiên, sự gia tăng dân số và hiện tượng Trái Đất nóng lên gây ảnh hưởng không nhỏ, khiến các rạn san hô bị suy thoái ở mức báo động. Nhiều yếu tố góp phần phá hủy chúng, có thể kể đến:\n\nĐánh bắt cá\n\nViệc đánh bắt cá quá mức trở thành vấn đề thực sự. Các kỹ thuật đánh bắt hiện đại không chỉ tăng sản lượng đánh bắt mà còn làm hỏng các rạn san hô trên đường tàu đi qua.\n\nSự nóng lên toàn cầu\n\nVới sự thay đổi tổng thể của khí hậu, đại dương cũng bị ảnh hưởng. Những thay đổi nhiệt độ nhỏ có thể khiến rạn san hô mất màu. Màu sắc bên ngoài này là dấu hiệu của san hô khỏe mạnh. Một khi chúng bắt đầu biến mất, san hô sẽ dễ bị cá ăn và tảo phá hủy.\n\nHoạt động dưới nước\n\nCác môn thể thao dưới nước như lặn với ống thở, lặn sâu trở nên phổ biển những năm trở lại đây. Ngoài yếu tố khám phá, mạo hiểm, nhiều người lặn chỉ để ngắm san hô. Tuy nhiên, thuyền của người lặn biển có thể neo trên một lớp san hô và nghiền nát chúng.\n\nNgoài ra, du khách dễ vô tình giẫm đạp san hô trong quá trình trải nghiệm. Những thợ lặn nghiệp dư, mới vào nghề không có nhiều khả năng kiểm soát dưới nước. Vây của họ dễ chạm vào san hô và gây ảnh hưởng đến sự phát triển của sinh vật này.\n\nChất thải\n\nPhế liệu nhựa, lưới và các chất thải khác trôi nổi ra biển là yếu tố hàng đầu làm hỏng hệ sinh thái dưới nước. Ngoài việc gây hại cho sinh vật biển, rác có thể vướng vào các rạn san hô, giết chết chúng.\n\nCải tạo đất\n\nĐể giải quyết vấn đề dân số ngày càng tăng, người dân đã bắt đầu khai hoang. Sự đảo lộn trật tự tự nhiên gây ra sự dịch chuyển của các chất dinh dưỡng cần thiết để san hô phát triển. Sự thay đổi này có thể giết chết san hô hoặc làm chúng xấu đi.\n\nDu khách nên làm gì?\nNhiều người tin rằng san hô được làm từ vật liệu cứng và phát triển dồi dào nên không dễ dàng bị phá hủy. Tuy nhiên, sinh vật này được bao bọc bởi một lớp màng rất mỏng và dễ vỡ khi chạm vào. Khi lớp màng bị đâm thủng, san hô dễ nhiễm trùng.\n\n\nbao ton ran san ho anh 3\nẢnh: Accor Hotels.\nVì vậy khi lặn, bạn cần chú ý:\n\nTránh tiếp xúc, giẫm đạp lên san hô.\nTuân thủ hướng dẫn lặn an toàn và không đi ra khỏi khu vực được cho phép.\nĐừng để thiết bị lặn (vây, bình dưỡng khí) vô tình chạm vào san hô.\nKhông mang theo các vật dụng nhỏ, đặc biệt là đồ nhựa. Chúng có thể rơi xuống nước.\nĐừng bẻ một ít san hô mang về nhà sau chuyến du lịch. Thay vào đó, bạn hãy chụp một bức ảnh làm kỷ niệm.','Song Nguyên',6,1),('2022-06-18 23:20:40','2022-06-20 08:52:46',17,'XÃ HỘI\nĐường sắt Cát Linh - Hà Đông lỗ nặng vì \'quá đơn độc\'','1','https://znews-photo.zingcdn.me/w960/Uploaded/yfsgs/2022_06_18/Ha_Noi_zig_14_1.jpg','Nếu không cẩn thận chạm vào san hô, chúng sẽ bị ảnh hưởng hoặc chết. Bỏ lại rác hay vô tình đánh rơi các vật dụng nhỏ cũng là yếu tố làm hại sinh vật này.','160 tỷ đồng là con số lỗ lũy kế khi vận hành tuyến đường sắt Cát Linh - Hà Đông, vừa được Công ty TNHH MTV đường sắt Hà Nội (Hanoi Metro) nêu ra tại báo cáo tài chính của doanh nghiệp.\n\nCâu chuyện tàu điện Cát Linh \"đã chạy là lỗ\" từng được các chuyên gia cảnh báo. Tuy nhiên, mức lỗ lên tới 160 tỷ đồng có nguyên nhân từ việc công trình chưa được Hà Nội trợ giá vận hành.\n\nKhách đông kỷ lục vẫn không đủ chi phí vận hành\nTrao đổi với Zing, ông Vũ Hồng Trường, Tổng giám đốc Hanoi Metro, cho biết với tính chất \"doanh thu không đảm bảo chi phí vận hành\", đường sắt Cát Linh - Hà Đông sẽ được Nhà nước trợ giá tương tự xe buýt.\n\n\nduong sat Cat Linh - Ha Dong thua lo anh 2\nLưu lượng khách đi tàu Cát Linh - Hà Đông đang ở mức 20.000-30.000 lượt/ngày. Ảnh: Thạch Thảo.\nTuy nhiên, đến nay Hanoi Metro vẫn vận hành với đơn giá tạm, chưa chính thức được TP Hà Nội đặt hàng. Ông Trường cho hay công ty đang trong quá trình hoàn thiện hồ sơ để thành phố đặt hàng cho tuyến Cát Linh - Hà Đông 2 tháng cuối năm 2021 và cả năm 2022.\n\n\"Sau khi doanh thu được cộng thêm trợ giá theo đặt hàng của thành phố, chắc chắn bức tranh tài chính sẽ khác. Trợ giá không chỉ bù đắp phần thiết hụt do doanh thu không đảm bảo chi phí, mà còn có lãi định mức theo quy định\", lãnh đạo Hanoi Metro chia sẻ.\n\nTheo báo cáo tài chính đã kiểm toán 2021 vừa được công bố, doanh thu của Hanoi Metro khi vận hành chính thức đoàn tàu chạy tuyến đường sắt Cát Linh - Hà Đông đạt hơn 5 tỷ đồng. Tuy nhiên, chi phí vận hành, quản lý khiến doanh nghiệp lỗ ròng 64 tỷ đồng.\n\nVào năm 2020, công ty báo lỗ 23 tỷ đồng khi chưa vận hành. Tính lũy kế, doanh nghiệp đang lỗ tổng cộng 160 tỷ đồng.\n\nChia sẻ với Zing trước đó, ông Vũ Hồng Trường từng khẳng định việc đường sắt đô thị thu không đủ bù chi là \"chuyện đương nhiên\" và phổ biến trên thế giới. Ông tiết lộ ngay cả ngày đông khách kỷ lục như dịp 30/4-1/5 (53.000 khách/ngày), doanh thu bán vé của tàu điện Cát Linh - Hà Đông cũng chưa đủ để bù đắp chi phí vận hành.\n\n\"Đốt tiền\" nếu chỉ chạy đơn tuyến\n\"Nếu làm đúng quy hoạch, giờ này Hà Nội đã có đến 4 tuyến đường sắt đô thị hoạt động rồi\", thạc sĩ Nguyễn Anh Tuấn (Đại học GTVT), nói với Zing.\n\n\nduong sat Cat Linh - Ha Dong thua lo anh 3\nViệc vận hành đơn tuyến khiến đường sắt Cát Linh - Hà Đông thua lỗ càng lớn. Ảnh: Hoàng Hà.\nÔng Tuấn khẳng định nếu tàu điện Cát Linh chỉ chạy một mình, không thể hiệu quả. Hiện nay, bình quân lượng khách đi tàu mỗi ngày chỉ đạt gần 30.000 lượt. Trong khi một tuyến xe buýt lớn cũng đạt khoảng 15.000 khách/ngày. Như vậy, tàu Cát Linh - Hà Đông chỉ đáp ứng được số \"chuyến đi\" bằng 2 tuyến buýt.\n\nTheo thiết kế, năng lực khai thác tối đa của tàu Cát Linh - Hà Đông là hơn một triệu người/ngày. Mỗi đoàn tàu có thể chở 960 người. \"Sản lượng hiện nay chưa đạt nổi 1/10 thì giải quyết được vấn đề gì? Mỗi ngày cả thành phố có 30 triệu chuyến đi mà tàu điện chỉ đáp ứng 30.000, đó là điều cần xem xét?\", ông Tuấn nêu vấn đề.\n\nÔng nhìn nhận đến nay, ý nghĩa của đường sắt Cát Linh - Hà Đông vẫn dừng lại ở mức độ cho người dân trải nghiệm, làm quen với loại hình vận tải công cộng văn minh.\n\nTheo quan điểm của chuyên gia, metro là loại hình vận tải cộng sinh, đơn tuyến sẽ không thể thu hút được hành khách. Do đó, sự chậm trễ, ì ạch của các dự án metro đang triển khai cũng góp phần kéo dài cả thời gian lẫn mức độ thua lỗ của tuyến Cát Linh - Hà Đông.\n\n\"Khi nào Hà Nội có được mạng lưới 4 tuyến metro ở 4 hướng nối nhau thì sản lượng của từng tuyến mới cải thiện được. Cát Linh - Hà Đông từ 30.000 khách có thể tăng lên 100.000 khách. Khi nào có 8 tuyến, sản lượng có thể vọt lên bằng công suất thiết kế\", thạc sĩ Nguyễn Anh Tuấn dự báo.\n\nThời điểm khai trương tuyến tàu điện Cát Linh - Hà Đông, Phó chủ tịch UBND Hà Nội Nguyễn Văn Quyền cho biết thành phố sẽ có 10 tuyến đường sắt đô thị với tổng chiều dài 417 km.\n\nTuy nhiên, tiến độ của các tuyến được triển khai sau Cát Linh - Hà Đông đều đang có vấn đề. Trong đó, dự án metro Nhổn - Ga Hà Nội đang gặp bế tắc phải dừng thi công hạng mục khoan hầm. Ban Quản lý dự án đường sắt đô thị Hà Nội (MRB) phải đề xuất tăng tổng mức đầu tư dự án Nhổn - ga Hà Nội thêm 4.905 tỷ đồng và lùi thời gian vận hành đến năm 2027.\n\nDự án metro số 1 Yên Viên - Ngọc Hồi sau nhiều năm được Ban quản lý dự án Đường sắt (Bộ GTVT) nghiên cứu đầu tư thì đến nay được bàn giao lại cho MRB. Tiến độ triển khai chưa được MRB cập nhật.\n\nTuyến đường sắt đô thị số 2 đoạn Nam Thăng Long - Trần Hưng Đạo vừa qua đội vốn từ 19.555 tỷ lên 35.679 tỷ đồng, trong khi vẫn chưa rõ ngày khởi công.','Ngọc Tân',21,1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `token` (
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `idUser` double DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiredAt` datetime DEFAULT NULL,
  `numberWrong` double DEFAULT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES ('2022-06-15 11:54:05','2022-06-15 11:54:11',34,1,'otp','866991','2022-06-15 11:55:04',9,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',1),('2022-06-15 11:56:41','2022-06-15 11:56:41',35,1,'otp','380587','2022-06-15 11:57:40',0,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',1),('2022-06-15 12:00:06','2022-06-15 12:00:23',38,1,'otp','640871','2022-06-15 12:01:05',2,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',1),('2022-06-15 14:21:42','2022-06-15 14:21:42',39,1,'otp','293398','2022-06-15 14:22:42',0,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',1),('2022-06-15 14:35:55','2022-06-15 14:35:55',40,1,'otp','460375','2022-06-15 14:36:55',0,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',1),('2022-06-15 14:39:22','2022-06-15 14:39:22',41,1,'otp','903572','2022-06-15 14:40:21',0,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',1),('2022-06-15 14:42:38','2022-06-15 14:42:38',44,1,'otp','799015','2022-06-15 14:43:38',0,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',1),('2022-06-15 14:51:14','2022-06-15 14:51:14',45,1,'otp','572592','2022-06-15 14:52:13',0,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',1),('2022-06-18 23:07:09','2022-06-18 23:07:54',46,1,'otp','506853','2022-06-18 23:08:08',1,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.124 Safari/537.36 Edg/102.0.1245.44',1),('2022-06-18 23:12:34','2022-06-18 23:12:48',47,1,'otp','264261','2022-06-18 23:13:34',2,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',1),('2022-06-18 23:14:21','2022-06-18 23:14:21',48,1,'otp','301492','2022-06-18 23:15:21',0,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:101.0) Gecko/20100101 Firefox/101.0',1);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('2022-06-13 09:57:35','2022-06-18 23:16:02',1,'turborvip','$2a$10$8w1gUM67tIGNRt7PFJbJhunHWTnbQii..ZAX.aS9EFRo9q.epgs5m','Do Thanh Dat','thanhdat.jitsinnovationlabs@gmail.com',1),('2022-06-13 15:09:25','2022-06-13 15:09:25',2,'turborvip2','$2a$10$py33Srspytv3VB.7g1cHn.qlXmcOvYng6FLOMvI6r8q5afh29c/Eq','Do Thanh Dat','turborvip@gmail.com',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'web_news'
--

--
-- Dumping routines for database 'web_news'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-20  9:52:34
